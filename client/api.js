import request from 'superagent'

const serverURL = 'http://localhost:3000/api/v1'


// create api get function
export function getData() {
  return request
    .get(`${serverURL}/charities`)
    .then(response => {
      // console.log(response)
      return response.text
    })
}