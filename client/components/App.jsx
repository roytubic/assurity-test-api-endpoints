import React from 'react'

import { getData } from '../api'

class App extends React.Component {
  state = {
    data: ''
  }

  componentDidMount () {
    getData()
      .then(data => this.setState({ data: data }))
  }
  render () {
    return (
      <p>{this.state.data}</p>
    )
  }
}

export default App
