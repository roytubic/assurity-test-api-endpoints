In order to run the test, you will need to clone the repo from the bitbucket respository to your machine.

Once done, you'll need to follow the following steps to run the test:
1. You'll need to run 'npm i' in the command terminal to install dependencies.
2. You'll need to install nock by running 'npm i nock' in the command terminal
3. You'll need to install jest by running 'npm i jest' in the command terminal. Once done, go to package.json and check that the "test" object under "scripts" has a string of "jest --watchall"
3. You'll need to install superagent for the code itself by running 'npm i superagent' in the command terminal
4. Once you have done this, you should be able to run the test by running 'npm run test' in the command terminal. This will run the test suite and show passing and failing tests.