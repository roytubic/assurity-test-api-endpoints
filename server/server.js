const path = require('path')
const express = require('express')

const request = require('superagent')
const server = express()

server.use(express.json())
server.use(express.static(path.join(__dirname, './public')))

module.exports = server

server.get('/api/v1/charities', (req,res)=>{
    request
    .get('https://api.tmsandbox.co.nz/v1/Categories/6328/Details.json?catalogue=false')    // api url 
    .then(response => {
        // console.log(response.text)
        let data = response.text
        // let parsed = JSON.parse(data)
        res.send(data)
        // console.log(data)
     })
})