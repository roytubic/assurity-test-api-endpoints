// import supertest from 'supertest'
import nock from 'nock'
// import of the function from the api js file.
import {getData} from '../client/api'


// Test 1 should determine that the Name element should be 'Badges'
describe('getName', () => {
    const mockName = {name: 'Badges'}
    const mockClassifieds = 'false'
    const mockTagline = {Charities: {Description: 'Plunket', Tagline: 'well child health services'}}
    const nameScope = nock('http://localhost:3000')
        .get('/api/v1/charities')
        .reply(201, mockName)
    const classifiedsScope = nock('http://localhost:3000')
    .get('/api/v1/charities')
    .reply(201, mockClassifieds)
    const taglineScope = nock('http://localhost:3000')
    .get('/api/v1/charities')
    .reply(201, mockTagline)

test("name should be Badges", () => {
    // call api function to retrieve the body response from the url
    return getData()
    .then(res => {
        expect(res).toEqual(mockName)
        expect(nameScope.isDone()).toBe(true)
        return null
        })
    })
// Test 2 should determine that the CanListClassifieds should be false
    test("CanlistClassifieds should be false", () => {
        // call api function to retrieve the body response from the url
        return getData()
        .then(res => {
            expect(res).toEqual(mockClassifieds)
            expect(classifiedsScope.isDone()).toBe(true)
            return null
            })
        })

// test if the plunket charities tagline includes 'well child health services'
    test("Plunket charity should have tagline that includes well child health services", () => {
        // call api function to retrieve the body response from the url
        return getData()
        .then(res => {
            expect(res).toHave(mockTagline) 
            // can't recall the expect function here toInclude doesn't work nor does toContain or toHave
            expect(classifiedsScope.isDone()).toBe(true)
            return null
            })
        })
})